package com.example.volgait2022.domain

import kotlinx.serialization.Serializable

@Serializable
data class TradeInput(
    val symbol: String,
    val type: String
)
