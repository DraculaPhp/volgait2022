package com.example.volgait2022.domain.usecase

import com.example.volgait2022.domain.Stock
import com.example.volgait2022.domain.StockRepository
import kotlinx.coroutines.flow.Flow

class GetStocksObserverUseCase(private val stockRepository: StockRepository) {
    fun execute(): Flow<List<Stock>> {
        return stockRepository.getStockFlow()
    }
}