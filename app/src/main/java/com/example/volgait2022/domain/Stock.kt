package com.example.volgait2022.domain

import java.io.Serializable

data class Stock(
    val symbol: String,
    val name: String,
    var price: Double,
) : Serializable
