package com.example.volgait2022.domain

import kotlinx.coroutines.flow.Flow


interface StockRepository {
    fun getStockFlow(): Flow<List<Stock>>

    suspend fun getStocks(): List<Stock>

    suspend fun getBySymbol(symbol: String): Stock?

    suspend fun updatePrice(symbol: String, price: Double)
}