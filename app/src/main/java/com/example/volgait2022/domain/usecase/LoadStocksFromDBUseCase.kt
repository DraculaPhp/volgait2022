package com.example.volgait2022.domain.usecase

import com.example.volgait2022.domain.Stock
import com.example.volgait2022.domain.StockRepository

class LoadStocksFromDBUseCase(
    private val stockRepository: StockRepository
) {
    suspend fun execute(): List<Stock> {
        return stockRepository.getStocks()
    }
}