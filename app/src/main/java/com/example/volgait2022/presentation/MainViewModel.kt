package com.example.volgait2022.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.example.volgait2022.data.network.StocksWebSocketListener
import com.example.volgait2022.data.room.Repositories
import com.example.volgait2022.domain.Stock
import com.example.volgait2022.domain.usecase.GetStocksObserverUseCase
import com.example.volgait2022.domain.usecase.LoadStocksFromDBUseCase
import kotlinx.coroutines.launch
import okhttp3.OkHttpClient
import okhttp3.Request
import java.util.concurrent.TimeUnit

class MainViewModel(
    getStocksObserverUseCase: GetStocksObserverUseCase = GetStocksObserverUseCase(Repositories.stocksRepositoryImpl),
    loadStocksFromDBUseCase: LoadStocksFromDBUseCase = LoadStocksFromDBUseCase(Repositories.stocksRepositoryImpl)
) : ViewModel() {

    companion object {
        private const val PING_INTERVAL = 30L
        private const val WEB_SOCKET_API_URL = "wss://ws.finnhub.io?token=c8hkq1iad3i9rgv9n8cg"
    }

    private var mapStocks = hashMapOf<String, Stock>()
    private val _stocks = MutableLiveData<List<Stock>>()
    val stocks: LiveData<List<Stock>>
        get() = _stocks
    var stocksObserver: LiveData<List<Stock>> = MutableLiveData(emptyList())

    init {
        stocksObserver = getStocksObserverUseCase.execute().asLiveData()
        viewModelScope.launch {
            _stocks.value = loadStocksFromDBUseCase.execute()
            _stocks.value?.forEach {
                mapStocks[it.symbol] = it
            }
            setWebSocketListener(mapStocks)
        }
    }

    private fun setWebSocketListener(mapStocks: Map<String, Stock>) {
        val request: Request = Request.Builder()
            .url(WEB_SOCKET_API_URL)
            .build()
        val stocksWebSocketListener = StocksWebSocketListener(mapStocks)
        val client = OkHttpClient.Builder()
            .pingInterval (PING_INTERVAL, TimeUnit.SECONDS)
            .build()
        client.newWebSocket(request, stocksWebSocketListener)
    }
}