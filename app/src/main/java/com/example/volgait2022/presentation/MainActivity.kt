package com.example.volgait2022.presentation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.volgait2022.data.room.Repositories
import com.example.volgait2022.databinding.ActivityMainBinding
import com.example.volgait2022.domain.Stock


@Suppress("UNCHECKED_CAST")
class MainActivity : AppCompatActivity() {

    private var layoutManager = LinearLayoutManager(this)

    private lateinit var binding: ActivityMainBinding

    private lateinit var mainViewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Repositories.init(applicationContext)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.stockListView.layoutManager = layoutManager
        binding.stockListView.addItemDecoration(
            DividerItemDecoration(binding.stockListView.context, DividerItemDecoration.VERTICAL)
        )

        mainViewModel = ViewModelProvider(this)[MainViewModel::class.java]

        mainViewModel.stocks.observe(this) {
            setList(it)
        }
        mainViewModel.stocksObserver.observe(this) {
            if (binding.stockListView.adapter != null) {
                val adapter = binding.stockListView.adapter as StockAdapter
                adapter.updateStocks(it)
            }
        }
    }

    private fun setList(stocks: List<Stock>) {
        binding.stockListView.adapter = StockAdapter(stocks)
    }
}