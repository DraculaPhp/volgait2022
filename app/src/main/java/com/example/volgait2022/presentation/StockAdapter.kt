package com.example.volgait2022.presentation

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.volgait2022.R
import com.example.volgait2022.domain.Stock

class StockAdapter(private var stocks: List<Stock>) : RecyclerView.Adapter<StockAdapter.StockViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StockViewHolder {
        val rootView = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.stock_item, parent, false)
        return StockViewHolder(rootView)
    }

    override fun onBindViewHolder(holder: StockViewHolder, position: Int) {
        holder.bind(stocks[position])
    }

    override fun getItemCount(): Int {
        return stocks.size
    }

    fun updateStocks(stocksList: List<Stock>) {
        DiffUtil
            .calculateDiff(StocksDiffUtils(stocks, stocksList))
            .dispatchUpdatesTo(this)
    }

    inner class StockViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val stockSymbol: TextView = itemView.findViewById(R.id.stockSymbol)

        private val stockPrice: TextView = itemView.findViewById(R.id.stockPrice)

        fun bind(stock: Stock) {
            val oldValue: Double = stockPrice.text.toString().toDouble()
            val newValue: Double = stock.price
            if (oldValue > newValue) {
                stockPrice.setTextColor(Color.parseColor("#ff0000"))
            } else if (oldValue < newValue) {
                stockPrice.setTextColor(Color.parseColor("#0000ff"))
            }
            stockPrice.text = newValue.toString()
            stockSymbol.text = stock.symbol
        }
    }
}

class StocksDiffUtils(
    private val oldList: List<Stock>,
    private val newList: List<Stock>
) : DiffUtil.Callback() {
    override fun getOldListSize(): Int {
        return oldList.size
    }

    override fun getNewListSize(): Int {
        return newList.size
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].symbol == newList[newItemPosition].symbol
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition] == newList[newItemPosition]
    }
}