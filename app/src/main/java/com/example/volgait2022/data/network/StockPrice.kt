package com.example.volgait2022.data.network

import com.google.gson.annotations.SerializedName

data class StockPrice(
    @SerializedName("c")
    var currentPrice: Double,
    @SerializedName("h")
    var highPrice: Double,
    @SerializedName("l")
    var lowPrice: Double,
    @SerializedName("o")
    var openPrice: Double,
    @SerializedName("pc")
    var prevClosePrice: Double
)
