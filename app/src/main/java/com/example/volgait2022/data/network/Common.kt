package com.example.volgait2022.data.network

object Common {
    private const val BASE_URL = "https://finnhub.io/"
    val retrofitService: RetrofitServices
        get() = RetrofitClient.getClient(BASE_URL).create(RetrofitServices::class.java)
}