package com.example.volgait2022.data.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.volgait2022.data.room.dao.StocksDao
import com.example.volgait2022.data.room.entities.StockDbEntity

@Database(
    version = 1,
    entities = [
        StockDbEntity::class
    ]
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun getStocksDao(): StocksDao
}