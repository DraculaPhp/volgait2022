package com.example.volgait2022.data.room.entities

data class StockUpdatePriceTuple(
    val symbol: String,
    val price: Double
)
