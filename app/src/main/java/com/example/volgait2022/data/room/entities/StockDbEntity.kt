package com.example.volgait2022.data.room.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.volgait2022.domain.Stock

@Entity(
    tableName = "stocks"
)
data class StockDbEntity(
    @PrimaryKey val symbol: String,
    val name: String,
    val price: Double
) {
    fun toStock(): Stock = Stock(
        symbol = symbol,
        name = name,
        price = price
    )
}
