package com.example.volgait2022.data.room

import android.content.Context
import androidx.room.Room

object Repositories {
    private lateinit var applicationContext: Context

    private val database: AppDatabase by lazy {
        Room.databaseBuilder(applicationContext, AppDatabase::class.java, "stocks.db")
            .createFromAsset("stocks_initial.db")
            .build()
    }

    val stocksRepositoryImpl: StocksRepositoryImpl by lazy {
        StocksRepositoryImpl(database.getStocksDao())
    }

    fun init(context: Context) {
        applicationContext = context
    }
}