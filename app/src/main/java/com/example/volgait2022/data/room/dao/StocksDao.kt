package com.example.volgait2022.data.room.dao

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Update
import com.example.volgait2022.domain.Stock
import com.example.volgait2022.data.room.entities.StockDbEntity
import com.example.volgait2022.data.room.entities.StockUpdatePriceTuple
import kotlinx.coroutines.flow.Flow

@Dao
interface StocksDao {

    @Query("SELECT * FROM stocks")
    fun getStockFlow(): Flow<List<Stock>>

    @Query("SELECT * FROM stocks")
    suspend fun getStocks(): List<Stock>

    @Query("SELECT * FROM stocks WHERE symbol = :symbol")
    suspend fun getBySymbol(symbol: String): Stock?

    @Update(entity = StockDbEntity::class)
    suspend fun updatePrice(stockUpdatePriceTuple: StockUpdatePriceTuple)
}