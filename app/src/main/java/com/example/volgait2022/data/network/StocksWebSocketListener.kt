package com.example.volgait2022.data.network

import android.util.Log
import com.example.volgait2022.data.room.Repositories
import com.example.volgait2022.domain.Stock
import com.example.volgait2022.domain.TradeInput
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import okhttp3.Response
import okhttp3.WebSocket
import okhttp3.WebSocketListener
import org.json.JSONObject
import kotlin.coroutines.CoroutineContext

class StocksWebSocketListener(
    private val mapStocks: Map<String, Stock>
) : WebSocketListener(), CoroutineScope {

    companion object {
        private const val DATA_KEY = "data"
        private const val SYMBOL_KEY = "s"
        private const val PRICE_KEY = "p"
        private const val MESSAGE_TYPE = "subscribe"
        private const val ON_MESSAGE_TAG = "onMessage"
        private const val ON_CLOSED_TAG = "onClosed"
        private const val ON_CLOSED_MESSAGE = "Connection is closed: "
        private const val ON_FAILURE_TAG = "onFailure"
        private const val ON_FAILURE_STACK = "Stack trace: "
        private const val ON_FAILURE_MESSAGE = "Error: "
    }

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO

    override fun onOpen(webSocket: WebSocket, response: Response) {
        super.onOpen(webSocket, response)
        mapStocks.forEach { (symbol, _) ->
            run {
                webSocket.send(Json.encodeToString(TradeInput(symbol, MESSAGE_TYPE)))
            }
        }
    }

    override fun onMessage(webSocket: WebSocket, text: String) {
        super.onMessage(webSocket, text)
        if (JSONObject(text).has(DATA_KEY)) {
            val jArr = JSONObject(text).getJSONArray(DATA_KEY)
            for (i in 0 until jArr.length()) {
                val jsonObject = jArr.getJSONObject(i)
                val symbol = jsonObject.getString(SYMBOL_KEY)
                val price = jsonObject.getDouble(PRICE_KEY)
                mapStocks[symbol]?.price = price
            }

            val listStocks = mapStocks
                .toSortedMap().values
                .toList()

            launch {
                listStocks.forEach {
                    Repositories.stocksRepositoryImpl.updatePrice(it.symbol, it.price)
                }
            }
        }
    }

    override fun onClosed(webSocket: WebSocket, code: Int, reason: String) {
        super.onClosed(webSocket, code, reason)
        Log.e(ON_CLOSED_TAG, ON_CLOSED_MESSAGE + reason)
    }

    override fun onFailure(webSocket: WebSocket, t: Throwable, response: Response?) {
        super.onFailure(webSocket, t, response)
        Log.e(ON_FAILURE_TAG, ON_FAILURE_MESSAGE + t.message.toString())
        Log.e(ON_FAILURE_TAG, ON_FAILURE_STACK + t.stackTraceToString())
    }
}