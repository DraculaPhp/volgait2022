package com.example.volgait2022.data.room

import com.example.volgait2022.data.room.dao.StocksDao
import com.example.volgait2022.domain.StockRepository
import com.example.volgait2022.domain.Stock
import com.example.volgait2022.data.room.entities.StockUpdatePriceTuple
import kotlinx.coroutines.flow.Flow

class StocksRepositoryImpl(
    private val stocksDao: StocksDao
) : StockRepository {
    override fun getStockFlow(): Flow<List<Stock>> {
        return stocksDao.getStockFlow()
    }

    override suspend fun getStocks(): List<Stock> {
        return stocksDao.getStocks()
    }

    override suspend fun getBySymbol(symbol: String): Stock? {
        return stocksDao.getBySymbol(symbol)
    }

    override suspend fun updatePrice(symbol: String, price: Double) {
        return stocksDao.updatePrice(
            StockUpdatePriceTuple(symbol, price)
        )
    }
}