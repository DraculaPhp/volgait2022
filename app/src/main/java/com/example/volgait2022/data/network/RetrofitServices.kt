package com.example.volgait2022.data.network

import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface RetrofitServices {
    @GET("api/v1/quote?token=c8hkq1iad3i9rgv9n8cg")
    @Headers("Content-Type: application/json")
    fun getStockPrice(@Query("symbol") symbol: String): Single<StockPrice>
}